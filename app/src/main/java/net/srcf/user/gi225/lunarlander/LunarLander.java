

/*
 * Copyright 2013 gi225
 *
 * fixed lunarlander example
 * added:
 * controls:
 * 		touchscreen
 * 		accelerometer
 * translations:
 * 		French
 * 		German
 * 		Hungarian
 *
 * modified
 *
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.srcf.user.gi225.lunarlander;

        import net.srcf.user.gi225.lunarlander.LunarView.LunarThread;
        import android.app.Activity;
        import android.content.Context;
        import android.content.SharedPreferences;
        import android.hardware.SensorManager;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.Display;
        import android.view.Menu;
        import android.view.MenuInflater;
        import android.view.MenuItem;
        import android.view.View;
        import android.view.WindowManager;
        import android.widget.TextView;

/**
 * the main activity
 *
 * @author gabor
 *
 */
public class LunarLander extends Activity {

    private SensorManager mSensorManager;
    private WindowManager mWindowManager;
    private Display mDisplay;

    /** A handle to the thread that's actually running the animation. */
    private LunarThread mLunarThread;

    /** A handle to the View in which the game is running. */
    private LunarView mLunarView;
    private TextView mTextView;

    /**
     * Invoked when the Activity is created.
     *
     * @param savedInstanceState
     *            a Bundle containing state saved from a previous execution, or
     *            null if this is a new execution
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mDisplay = mWindowManager.getDefaultDisplay();
        // tell system to use the layout defined in our XML file
        setContentView(R.layout.lunar_layout);

        // get handles to the LunarView from XML, and its LunarThread
        mLunarView = (LunarView) findViewById(R.id.lunar);
        mLunarThread = mLunarView.getThread();

        mTextView = (TextView) findViewById(R.id.text);

        mLunarView.setSensorManager(mSensorManager);
        mLunarView.setDisplay(mDisplay);
        // give the LunarView a handle to the TextView used for messages
        // mLunarView.setTextView((TextView) findViewById(R.id.text));
        mLunarView.setDependentViews(mTextView,
                findViewById(R.id.arrowContainer));

        if (savedInstanceState == null) {
            // we were just launched: set up a new game
            mLunarThread.setState(LunarThread.STATE_READY);
            Log.w(this.getClass().getName(), "SIS is null");
        } else {
            // we are being restored: resume a previous game
            mLunarThread.restoreState(savedInstanceState);
            Log.w(this.getClass().getName(), "SIS is nonnull");
        }
    }

    /**
     * Invoked during init to give the Activity a chance to set up its Menu.
     *
     * @param menu
     *            the Menu to which entries may be added
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);

        SharedPreferences sharedPref = getSharedPreferences(getResources()
                .getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        int difficulty = sharedPref.getInt(
                getResources().getString(R.string.saved_difficulty), LunarThread.DIFFICULTY_MEDIUM);
        switch (difficulty) {
            case LunarThread.DIFFICULTY_EASY:
                menu.findItem(R.id.menu_easy).setChecked(true);
                break;
            case LunarThread.DIFFICULTY_MEDIUM:
                menu.findItem(R.id.menu_medium).setChecked(true);
                break;
            case LunarThread.DIFFICULTY_HARD:
                menu.findItem(R.id.menu_hard).setChecked(true);
                break;
        }
        int input = sharedPref.getInt(
                getResources().getString(R.string.saved_input), LunarThread.INPUT_TOUCHSCREEN);
        switch (input) {
            case LunarThread.INPUT_ACCELEROMETER:
                menu.findItem(R.id.menu_accelerometer).setChecked(true);
                break;
            case LunarThread.INPUT_KEYBOARD:
                menu.findItem(R.id.menu_keyboard).setChecked(true);
                break;
            case LunarThread.INPUT_TOUCHSCREEN:
                menu.findItem(R.id.menu_touchscreen).setChecked(true);
                break;
        }
        return true;
    }

    /**
     * Invoked when the user selects an item from the Menu.
     *
     * @param item
     *            the Menu entry which was selected
     * @return true if the Menu item was legit (and we consumed it), false
     *         otherwise
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.isChecked())
            item.setChecked(false);
        else
            item.setChecked(true);
        switch (item.getItemId()) {
            case R.id.menu_start:
                mLunarThread.doStart();
                return true;
            case R.id.menu_stop:
                mLunarThread.setState(LunarThread.STATE_LOSE,
                        getText(R.string.message_stopped));
                return true;
            case R.id.menu_pause:
                mLunarThread.pause();
                return true;
            case R.id.menu_resume:
                mLunarThread.unpause();
                return true;
            case R.id.menu_easy:
                mLunarThread.setDifficulty(LunarThread.DIFFICULTY_EASY);
                return true;
            case R.id.menu_medium:
                mLunarThread.setDifficulty(LunarThread.DIFFICULTY_MEDIUM);
                return true;
            case R.id.menu_hard:
                mLunarThread.setDifficulty(LunarThread.DIFFICULTY_HARD);
                return true;
            case R.id.menu_keyboard:
                mLunarThread.setInput(LunarThread.INPUT_KEYBOARD);
                return true;
            case R.id.menu_touchscreen:
                mLunarThread.setInput(LunarThread.INPUT_TOUCHSCREEN);
                return true;
            case R.id.menu_accelerometer:
                mLunarThread.setInput(LunarThread.INPUT_ACCELEROMETER);
                return true;
            case R.id.menu_highscore:
                SharedPreferences sharedPref = getSharedPreferences(getResources()
                                .getString(R.string.preference_file_key),
                        Context.MODE_PRIVATE);
                int highScore = sharedPref.getInt(
                        getResources().getString(R.string.saved_highscore), 0);
                mTextView.setVisibility(View.VISIBLE);
                mTextView.setText(getResources().getText(R.string.highscore) + " "
                        + Integer.toString(highScore)
                        + getResources().getText(R.string.hs_continue));
                return true;
            case R.id.menu_whatsnew:
                mTextView.setVisibility(View.VISIBLE);
                mTextView.setText(getResources().getText(R.string.whatsnew));
                return true;
            case R.id.menu_developermode:
                mLunarThread.toggleDeveloperMode();
                return true;
        }

        return false;
    }

    /**
     * Invoked when the Activity loses user focus.
     */
    @Override
    protected void onPause() {
        super.onPause();
        mLunarView.stopSimulation();
        mLunarView.getThread().pause(); // pause game when Activity pauses
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Start the simulation
        mLunarView.startSimulation();
    }

    /**
     * Notification that something is about to happen, to give the Activity a
     * chance to save state.
     *
     * @param outState
     *            a Bundle into which this Activity should save its state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // just have the View's thread save its state into our Bundle
        super.onSaveInstanceState(outState);
        mLunarThread.saveState(outState);
        Log.w(this.getClass().getName(), "SIS called");
    }
}

